#!/bin/bash
set -x

mkdir -p ~/bin

apt-get install -y --no-install-recommends \
    apt-transport-https \
    autoconf \
    bc \
    build-essential \
    ca-certificates \
    chrome-gnome-shell \
    curl \
    dh-autoreconf \
    gettext \
    git \
    gnome-tweak-tool \
    gnupg-agent \
    gnupg2 \
    graphviz \
    htop \
    hunspell \
    i2c-tools \
    jq \
    libbz2-dev \
    libcairo2 \
    libcurl4-openssl-dev \
    libffi-dev \
    libfreetype6-dev \
    libgdk-pixbuf2.0-0 \
    libgraphviz-dev \
    libhunspell-dev \
    libjpeg8-dev \
    liblcms2-dev \
    libldap2-dev \
    liblzma-dev \
    libncurses5-dev \
    libpango-1.0-0 \
    libpangocairo-1.0-0 \
    libpq-dev \
    libsasl2-dev \
    libssl-dev \
    libtiff5-dev \
    libwebp-dev \
    network-manager-openvpn \
    network-manager-openvpn-gnome \
    openresolv \
    openssh-client \
    openvpn \
    pkg-config \
    python-is-python3 \
    python3 \
    python3-dev \
    python3-pip \
    python3-venv \
    shared-mime-info \
    software-properties-common \
    ubuntu-restricted-extras \
    unixodbc \
    unixodbc-dev \
    vlc \
    wget \
    zlib1g-dev



# Setup ssh
ssh-keygen -t ed25519 -f ~/.ssh/id_ed25519 -q -N ''

# Docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
apt-key fingerprint 0EBFCD88
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update
apt-get -y install docker-ce docker-ce-cli containerd.io
groupadd docker
usermod -aG docker $USER

# Minikube
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
dpkg -i minikube_latest_amd64.deb

# Kubectl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | tee /etc/apt/sources.list.d/kubernetes.list
apt-get update
apt-get install -y kubectl

# Python
pip3 install --upgrade pip virtualenv setuptools wheel cython docker-compose cffi python-language-server[all]

# Sublime Text
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | apt-key add -
echo "deb https://download.sublimetext.com/ apt/stable/" | tee /etc/apt/sources.list.d/sublime-text.list
apt-get update
apt-get install sublime-text

# Pycharm
snap install pycharm-professional --classic
# snap install pycharm-community --classic

# Chrome
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
dpkg -i google-chrome-stable_current_amd64.deb

# Slack
snap install slack --classic

# Telegram
snap install telegram-desktop

# Ubuntu repositories
add-apt-repository "deb http://archive.canonical.com/ubuntu $(lsb_release -sc) partner"
apt-get update

apt-get -y dist-upgrade
apt-get -y autoclean
apt-get -y autoremove
