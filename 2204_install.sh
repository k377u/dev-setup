#!/bin/bash
set -x

mkdir -p ~/bin


sudo add-apt-repository multiverse

sudo apt update

sudo apt install -y --no-install-recommends \
    apt-transport-https \
    autoconf \
    bc \
    build-essential \
    ca-certificates \
    curl \
    dh-autoreconf \
    gettext \
    git \
    git-lfs \
    gnupg-agent \
    gnupg2 \
    graphviz \
    htop \
    jq \
    lsb-release \
    libbz2-dev \
    libffi-dev \
    libgraphviz-dev \
    libnuma-dev \
    libpq-dev \
    libsasl2-dev \
    libssl-dev \
    libtiff5-dev \
    libwebp-dev \
    nfs-kernel-server \
    network-manager-openvpn \
    network-manager-openvpn-gnome \
    openresolv \
    openssh-client \
    openvpn \
    pkg-config \
    python-is-python3 \
    python3 \
    python3-dev \
    python3-pip \
    python3-virtualenv \
    ubuntu-restricted-extras \
    wget \
    zlib1g-dev

sudo apt update



# If specific kernel version needed: ubuntu 22.04.1 5.15.0-46-generic
# sudo apt install linux-headers-`uname -r` linux-modules-extra-`uname -r`

wget -qO - https://repo.radeon.com/rocm/rocm.gpg.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/rocm.gpg > /dev/null
echo 'deb [arch=amd64] https://repo.radeon.com/amdgpu/5.3/ubuntu jammy main' | sudo tee /etc/apt/sources.list.d/amdgpu.list
echo 'deb [arch=amd64] https://repo.radeon.com/rocm/apt/5.3 jammy main' | sudo tee /etc/apt/sources.list.d/rocm.list


# radeon 6900xt miopenkernels-gfx1030-36kdb
sudo apt install amdgpu-dkms rocm-dkms miopenkernels-gfx1030-36kdb amdgpu-install amdgpu-dkms-firmware amdgpu-dkms-firmware amdgpu-dkms amdgpu-install
sudo usermod -a -G video $USER
sudo usermod -a -G render $USER

sudo amdgpu-install --usecase=dkms,graphics,rocm,lrt,hip,hiplibsdk,opencl,openclsdk --opencl=rocr --accept-eula --no-32

# Test rocm
# /opt/rocm-5.3.0/bin/rocminfo
# /opt/rocm-5.3.0/opencl/bin/clinfo


sudo apt install docker.io
sudo usermod -aG docker $USER


wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome-stable_current_amd64.deb


sudo snap install pycharm-community slack --classic


# Sublimetext
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/sublimehq-archive.gpg > /dev/null
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
sudo apt update
sudo apt install sublime-tex


sudo apt -y dist-upgrade
sudo apt -y autoclean
sudo apt -y autoremove
