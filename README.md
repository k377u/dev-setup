






# Git branch to shell

~/.bashrc
```
parse_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]\[\e[91m\]$(parse_git_branch)\[\e[00m\]:\n\[\033[01;34m\]\w \[\e[00m\]$ '
fi
```

# RGB

https://gitlab.com/CalcProgrammer1/OpenRGB


sudo apt install i2c-tools

i2cdetect


sudo modprobe i2c-dev
modprobe i2c-piix4